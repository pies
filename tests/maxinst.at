# This file is part of GNU pies testsuite. -*- autotest -*-
# Copyright (C) 2019-2025 Sergey Poznyakoff
#
# GNU pies is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU pies is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU pies.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([inet component: max instances])
AT_CHECK([
PIES_XFAIL_CHECK
PIES_CONTROL_INIT

AT_DATA([in1],
[test
enable con2
sleep 10
quit
])

AT_DATA([in2],
[test
enable con3
sleep 10
quit
])

: ${PIES_TEST_INET_SOCKET:=unix://$PWD/in.sock}

cat > pies.conf <<_EOT
component in {
  command "$auxdir/in.test $PWD/in.log";
  mode inetd;
  socket "$PIES_TEST_INET_SOCKET";
  stderr file "$PWD/in.err";
  max-instances 2;
  max-instances-message "too many instances running\n";
}
component con1 {
  command "nt -i in1 -o con1.out $PIES_TEST_INET_SOCKET";
  stderr file "/tmp/con1.err";
}
component con2 {
  command "nt -i in2 -o con2.out $PIES_TEST_INET_SOCKET";
  stderr file "/tmp/con2.err";
  flags (disable);
}
component con3 {
  flags (disable);
  command "nt -o con3.out $PIES_TEST_INET_SOCKET";
  stderr file "/tmp/con3.err";
  return-code * {
     action disable;
     exec "$abs_top_builddir/src/piesctl --url unix://$PWD/pies.ctl --config-file=/dev/null --no-netrc shutdown";
  }
}
_EOT

set -e
to 10 \
  pies --no-preprocessor --foreground --stderr \
       --config-file control.conf --config-file pies.conf --debug 1 2>errlog

cat con3.out
],
[0],
[too many instances running
])

AT_CLEANUP
