/* This file is part of GNU Pies.
   Copyright (C) 2007-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#define PIES_LOG_BUF_SIZE 1024
#define PIES_LOG_DEV "/dev/log"
#define PIES_LOG_MAX_QUEUE 64

int pies_syslog_set_dev (char const *dev);
int pies_syslog_open (void);
void pies_syslog_close (void);
void pies_syslog (int pri, char const *fmt, ...);
void pies_vsyslog (int pri, char const *fmt, va_list ap);
void pies_syslog_flush (void);
void pies_syslog_message (int prio, char const *text, char const *tag, pid_t pid);

extern char *pies_fallback_file;
extern char *pies_log_tag;
extern int pies_log_facility;
extern size_t pies_log_max_queue;





