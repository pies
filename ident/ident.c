/* This file is part of GNU Pies.
   Copyright (C) 2015-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include "ident.h"

pies_identity_t
pies_identity_create (char const *user)
{
  pies_identity_t id = malloc (sizeof (*id));
  if (id)
    {
      id->provider = NULL;
      id->data = NULL;
      id->username = strdup (user);
      if (!id)
	{
	  int ec = errno;
	  free (id);
	  errno = ec;
	  id = NULL;
	}
    }
  return id;
}
    
int
pies_authenticate (pies_identity_provider_t pr, pies_identity_t id,
		   char const *passwd)
{
  if (!pr || !id)
    return -1;

  if (pr->mech->authenticate (pr, id, passwd) == 0)
    {
      id->provider = pr;
      return 0;
    }
  return 1;
}

char const *
pies_identity_user_name (pies_identity_t id)
{
  if (!id)
    return NULL;
  return id->username;
}

int
pies_identity_is_user (pies_identity_t id, char * const * users)
{
  if (!id)
    return 0;
  return is_array_member (users, id->username);
}

int
pies_identity_is_group_member (pies_identity_t id, char * const * groups)
{
  pies_identity_provider_t provider;
  if (!id)
    return 0;
  provider = id->provider;
  if (!provider)
    return 0;
  return provider->mech->is_group_member (provider, id, groups);
}

void
pies_identity_destroy (pies_identity_t id)
{
  pies_identity_provider_t provider = id->provider;
  if (provider && provider->mech->destroy_identity)
    provider->mech->destroy_identity (provider, id);
  free (id);
}


  
