/* This file is part of GNU Pies.
   Copyright (C) 2014-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include "pies.h"

#if defined __linux__
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/kd.h>
#include <sys/reboot.h>

void
sysvinit_sysdep_begin (void)
{
	int fd;
	
	reboot (RB_DISABLE_CAD);
	fd = open (console_device, O_RDWR | O_NOCTTY);
	if (fd != -1) {
		ioctl (fd, KDSIGACCEPT, SIGWINCH);
		close (fd);
	} else
		ioctl (0, KDSIGACCEPT, SIGWINCH);
}
		
#else
void
sysvinit_sysdep_begin (void)
{
}
#endif
