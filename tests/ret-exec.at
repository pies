# This file is part of GNU pies testsuite. -*- autotest -*-
# Copyright (C) 2016-2025 Sergey Poznyakoff
#
# GNU pies is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU pies is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU pies.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Exec on return code])
AT_KEYWORDS([ret-exec])

AT_CHECK([
PIES_XFAIL_CHECK
PIES_CONTROL_INIT
comp_pid_file=$PWD/comp.pid
report_file=$PWD/report
cat > pies.conf <<_EOT
component test {
	mode respawn;
	return-code 10 {
		exec "$auxdir/retcode $report_file";
		action disable;
	}
	command "$auxdir/sleepexit 2 10 $comp_pid_file";
}
_EOT

>$report_file

pies --no-preprocessor --config-file control.conf --config-file pies.conf

n=0
while test ! -s $report_file
do
    sleep 1
    n=$(($n + 1))
    if test $n -gt 4; then
        echo >&2 "timed out"
        break
    fi
done

PIES_STOP

if test -f $report_file; then
    pid=`head $comp_pid_file`
    sed "s/$pid/PID/" $report_file
else
    echo >&2 "no report file"
fi    
],
[0],
[AT_PACKAGE_VERSION
test
PID
10
No signal
])

AT_CLEANUP
