/* This file is part of GNU Pies testsuite.
   Copyright (C) 2019-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdio.h>
#include <signal.h>
#include <assert.h>
#include <unistd.h>

static void
sighan (int signo)
{
}

int
main (int argc, char **argv)
{
  sigset_t set;
  struct sigaction act;
  int c, n;
  FILE *fp = stdout;
  char const *program_name = argv[0];
  char const *file_name = NULL;
  
  while ((c = getopt (argc, argv, "n:f:")) != EOF)
    {
      switch (c)
	{
	case 'n':
	  program_name = optarg;
	  break;

	case 'f':
	  file_name = optarg;
	  break;

	default:
	  return 1;
	}
    }
  assert (argc == optind);
  
  act.sa_flags = 0;
  sigemptyset (&act.sa_mask);
  act.sa_handler = sighan;
  sigaction (SIGTERM, &act, NULL);
  sigaction (SIGQUIT, &act, NULL);
  sigfillset (&set);
  assert (sigwait (&set, &n) == 0);
  if (file_name)
    {
      if ((fp = fopen (file_name, "a")) == NULL)
	perror (file_name);
      assert (lockf (fileno (fp), F_LOCK, 0) == 0);
    }
  fprintf (fp, "%s: %d\n", program_name, n);
  fclose (fp);
  return 0;
}

  
  
