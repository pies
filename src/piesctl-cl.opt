/* This file is part of GNU Pies. -*- c -*-
   Copyright (C) 2008-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

char *incpath = DEFAULT_INCLUDE_PATH;

OPTIONS_BEGIN("piesctl",
              [<GNU pies control program>],
	      [<COMMAND [ARG...]>],
	      [<gnu>],
              [<copyright_year=2008-2023>],
              [<copyright_holder=Sergey Poznyakoff>])

OPTION(instance,i,NAME,
       [<connect to instance NAME>])
BEGIN
  instance = optarg;
END

OPTION(config-file,c,FILE,
       [<use FILE instead of the default configuration>])
BEGIN
  config_file = optarg;
END

OPTION(config-help,,,
       [<show configuration file summary>])
BEGIN
  config_help ();
  exit (0);
END

OPTION(,E,,
       [<preprocess configuration files and exit>])
BEGIN
  preprocess_only = 1;
END       

OPTION(verbose,v,,
       [<verbose diagnostics>])
BEGIN
  ++verbose;
END

OPTION(dump,d,,
       [<dump obtained responses verbatim>])
BEGIN
  ++dump;
END

OPTION(url,u,URL,
       [<connect to this socket>])
BEGIN
  if (pies_url_create (&client.url, optarg))
    {
      grecs_error (NULL, 0, _("%s: cannot create URL: %s"),
		   optarg, strerror (errno));
      exit (EX_USAGE);
    }
END  

OPTION(no-netrc,N,,
       [<don't read ~/.netrc file>])
BEGIN
  no_netrc_option = 1;
END

GROUP(Preprocessor)

OPTION(preprocessor,,COMMAND,
       [<use COMMAND instead of the default preprocessor>])
BEGIN
    if (DEFAULT_PREPROCESSOR)
        grecs_preprocessor = optarg;
END

OPTION(no-preprocessor,,,
       [<disable preprocessing>])
BEGIN
    grecs_preprocessor = NULL;
END

OPTION(include-directory,I,DIR,
       [<add include directory>])
BEGIN
  pp_add_option ("-I", optarg);
END

OPTION(no-include,,,
       [<clear default preprocessor search path>])
BEGIN
  incpath = NULL;
END

OPTION(define,D,[<NAME[=VALUE]>],
       [<define a preprocessor symbol NAME as having VALUE or empty>])
BEGIN
  pp_add_option ("-D", optarg);
END
       
OPTION(undefine,U,NAME,
       [<undefine a preprocessor symbol NAME>])
BEGIN
  pp_add_option ("-U", optarg);
END


OPTIONS_END

void
parse_options (int argc, char *argv[], int *index)
{
  GETOPT(argc, argv, *index);
  pp_init (incpath);
  setenv ("PIES_INSTANCE", instance, 1);
}


	      
