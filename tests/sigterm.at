# This file is part of GNU pies testsuite. -*- autotest -*-
# Copyright (C) 2022-2025 Sergey Poznyakoff
#
# GNU pies is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU pies is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU pies.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([sigterm])
AT_KEYWORDS([sigterm])
AT_CHECK([
PIES_XFAIL_CHECK
PIES_CONTROL_INIT

resultfile=$PWD/result

cat > pies.conf <<_EOT
component test {
  mode respawn;
  command "sigwait -f$resultfile";
  sigterm SIGQUIT;
}
component sentinel {
  mode respawn;
  prerequisites (test);
  command "piesctl --config-file=/dev/null --url unix:///$PWD/pies.ctl --no-netrc shutdown";
}
_EOT

rm -f result
set -e
to 5 \
  pies --no-preprocessor --foreground --stderr \
       --config-file control.conf --config-file pies.conf --debug 1 2>errlog
cat result
],
[0],
[sigwait: 3
])

AT_CLEANUP