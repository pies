/* This file is part of GNU Pies.
   Copyright (C) 2009-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <gettext.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <grecs.h>

#if defined HAVE_SYSCONF && defined _SC_OPEN_MAX
# define getmaxfd() sysconf(_SC_OPEN_MAX)
#elif defined (HAVE_GETDTABLESIZE)
# define getmaxfd() getdtablesize()
#else
# define getmaxfd() 256
#endif

void pies_close_fds (int minfd);

void mf_proctitle_init (int argc, char *argv[], char *env[]);
void mf_proctitle_format (const char *fmt, ...);

size_t longtostr (long i, char *buf, size_t size);
size_t ulongtostr (unsigned long i, char *buf, size_t size);

struct tokendef
{
  char *name;
  int tok;
};

int strtotok_len (struct tokendef *tab, const char *str, size_t len,
		  int *pres);
int strtotok_len_ci (struct tokendef *tab, const char *str, size_t len,
		     int *pres);
int strtotok (struct tokendef *tab, const char *str, int *pres);
int strtotok_ci (struct tokendef *tab, const char *str, int *pres);
int toktostr (struct tokendef *tab, int tok, const char **pres);

int is_array_member (char * const * ar, char const *str);
int array_index (char * const *ar, char const *str);
int strsplit3 (const char *input, char *result[3], int flag);
int safe_strcmp (char const *a, char const *b);

/* url.c */
struct pies_url
{
  char *string;
  char *scheme;
  char *host;
  char *port_s;
  int port;
  char *proto_s;
  int proto;
  char *path;
  char *user;
  char *passwd;
  int argc;
  char **argv;
};

int pies_basic_url_create (struct pies_url **purl, const char *str);
int pies_url_create (struct pies_url **purl, const char *str);
void pies_url_destroy (struct pies_url **purl);
const char *pies_url_get_arg (struct pies_url *url, const char *argname);
int pies_url_copy (struct pies_url **purl, struct pies_url *src);
void pies_url_free_user (struct pies_url *url);
void pies_url_free_passwd (struct pies_url *url);

void netrc_scan (struct pies_url *url);


/* pp.c */
void pp_init (char const *path);
void pp_add_option (const char *opt, const char *arg);
char *pp_command_line (void);

char *mkfilename (const char *dir, const char *name, const char *suf);

union pies_sockaddr_storage
{
  struct sockaddr s;
  struct sockaddr_in s_in;
  struct sockaddr_un s_un;
};

/* addrfmt.c */
void sockaddr_to_str (const struct sockaddr *sa, int salen,
		      char *bufptr, size_t buflen,
		      size_t *plen);
char *sockaddr_to_astr (const struct sockaddr *sa, int salen);

/* urlconn.c */
int url_connect (struct pies_url *url, struct grecs_sockaddr *source_addr);



