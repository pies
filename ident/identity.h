/* This file is part of GNU Pies.
   Copyright (C) 2015-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

typedef struct pies_identity *pies_identity_t;
typedef struct pies_identity_provider *pies_identity_provider_t;
typedef struct pies_identity_mechanism *pies_identity_mechanism_t;

pies_identity_t pies_identity_create (char const *user);
void pies_identity_destroy (pies_identity_t id);
int pies_identity_is_user (pies_identity_t id, char * const * users);
int pies_identity_is_group_member (pies_identity_t id, char * const * groups);
char const *pies_identity_user_name (pies_identity_t p);

int pies_authenticate (pies_identity_provider_t pr, pies_identity_t id,
		       char const *passwd);
char const *pies_identity_provider_name (pies_identity_provider_t p);

int pies_identity_mechanism_register (pies_identity_mechanism_t mech);
void pies_config_identity_mechanisms_help (void);
int pies_config_provider (struct grecs_node *node);

extern struct pies_identity_mechanism system_identity_mechanism;
#ifdef WITH_PAM
extern struct pies_identity_mechanism pam_identity_mechanism;
#endif
extern struct grecs_list *identity_provider_list;
