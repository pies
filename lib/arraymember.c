/* This file is part of GNU Pies.
   Copyright (C) 2015-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <string.h>
#include "libpies.h"

int
is_array_member (char * const *ar, char const *str)
{
  for (; *ar; ++ar)
    {
      if (strcmp (*ar, str) == 0)
	return 1;
    }
  return 0;
}

int
array_index (char * const *ar, char const *str)
{
  int i;
  
  for (i = 0; ar[i]; i++)
    {
      if (strcmp (ar[i], str) == 0)
	return i;
    }
  return -1;
}

