/* This file is part of GNU Pies.
   Copyright (C) 2009-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <string.h>
#include "libpies.h"
#include "c-strcase.h"

int
strtotok_len (struct tokendef *tab, const char *str, size_t len, int *pres)
{
  for (; tab->name; tab++)
    {
      size_t kwlen = strlen (tab->name);
      if (kwlen == len && memcmp (tab->name, str, len) == 0)
	{
	  *pres = tab->tok;
	  return 0;
	}
    }
  return 1;
}

int
strtotok_len_ci (struct tokendef *tab, const char *str, size_t len, int *pres)
{
  for (; tab->name; tab++)
    {
      size_t kwlen = strlen (tab->name);
      if (kwlen == len && c_strncasecmp (tab->name, str, len) == 0)
	{
	  *pres = tab->tok;
	  return 0;
	}
    }
  return 1;
}

int
strtotok (struct tokendef *tab, const char *str, int *pres)
{
  return strtotok_len (tab, str, strlen (str), pres);
}

int
strtotok_ci (struct tokendef *tab, const char *str, int *pres)
{
  return strtotok_len_ci (tab, str, strlen (str), pres);
}

int
toktostr (struct tokendef *tab, int tok, const char **pres)
{
  for (; tab->name; tab++)
    if (tab->tok == tok)
      {
	*pres = tab->name;
	return 0;
      }
  return 1;
}  
