/* This file is part of GNU Pies testsuite.
   Copyright (C) 2019-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdio.h>

int
main (int argc, char **argv)
{
  char *progname = argv[0];
  FILE *fp;
  int c;
  unsigned count;
  
  if (argc != 2)
    {
      fprintf (stderr, "usage: %s FILE\n", progname);
      fprintf (stderr, "Prints number of lines in FILE.\n");
      return 1;
    }

  fp = fopen (argv[1], "r");
  if (!fp)
    {
      perror (argv[1]);
      return 2;
    }

  count = 0;
  while ((c = fgetc (fp)) != EOF)
    if (c == '\n')
      count++;
  fclose (fp);
  printf ("%u\n", count);
  return 0;
}
