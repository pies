/* This file is part of GNU Pies.
   Copyright (C) 2015-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <grecs.h>
#include <sysexits.h>
#include <wordsplit.h>

static struct grecs_txtacc *pp_cmd;
static char const *pp_path;

void
pp_init (char const *path)
{
  pp_path = path;
}

void
pp_add_option (const char *opt, const char *arg)
{
  if (!pp_cmd)
    {
      if (!grecs_preprocessor)
	{
	  grecs_error (NULL, 0, _("no preprocessor configured"));
	  exit (EX_CONFIG);
	}
      pp_cmd = grecs_txtacc_create ();
    }
  grecs_txtacc_grow_char (pp_cmd, ' ');
  grecs_txtacc_grow_string (pp_cmd, opt);
  
  if (arg)
    {
      grecs_txtacc_grow_char (pp_cmd, '\'');
      for (; *arg; ++arg)
	{
	  if (*arg == '\'')
	    grecs_txtacc_grow_string (pp_cmd, "'\\''");
	  else
	    grecs_txtacc_grow (pp_cmd, arg, 1);
	}
      grecs_txtacc_grow_char (pp_cmd, '\'');
    }
}

/*
 * Build and return the preprocessor command line.
 */
char *
pp_command_line (void)
{
  char *ret;
  size_t len, pplen;
  
  if (!pp_cmd)
    {
      if (!grecs_preprocessor)
	return NULL;

      /* Initialize pp_cmd */
      pp_cmd = grecs_txtacc_create ();
    }

  /* Append the default search path options. */
  if (pp_path)
    {
      struct wordsplit ws;
      ws.ws_delim = ":";
      if (wordsplit (pp_path, &ws,
		     WRDSF_DELIM |
		     WRDSF_ENOMEMABRT |
		     WRDSF_SHOWERR |
		     WRDSF_NOCMD |
		     WRDSF_NOVAR) == 0)
	{
	  size_t i;
	  for (i = 0; i < ws.ws_wordc; i++)
	    pp_add_option ("-I", ws.ws_wordv[i]);
	}
      wordsplit_free (&ws);
    }

  /*
   * Insert preprocessor command before its options.
   * Since grecs_txtacc interface makes no provision for inserting text
   * at the beginning, this is done as follows:
   *
   *  1. Terminating \0 is appended.
   *  2. The preprocessor command line is appended *to the end* of the
   *     buffer to make sure sufficient space is allocated.
   *  3. The txtacc is finalized.  This returns allocated array of
   *     characters with the preprocessor name appended after the \0
   *     character.
   *  4. The resulting array is shifted N bytes to the right, where
   *     N is the length of the preprocessor command plus one.  Now
   *     terminating \0 occupies the last byte of the allocated memory.
   *  5. Preprocessor command is placed at the beginning of the array,
   *     filling in the space provided in previous step.
   */
  
  grecs_txtacc_grow_char (pp_cmd, 0);
  grecs_txtacc_grow_string (pp_cmd, grecs_preprocessor);
  ret = grecs_txtacc_finish (pp_cmd, 1);

  len = strlen (ret);
  pplen = strlen (grecs_preprocessor);
  memmove (ret + pplen, ret, len + 1);
  memcpy (ret, grecs_preprocessor, pplen);
  
  grecs_txtacc_free (pp_cmd);
  pp_cmd = NULL;
  return ret;
}
