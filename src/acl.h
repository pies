/* This file is part of GNU Pies
   Copyright (C) 2009-2025 Sergey Poznyakoff
  
   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

/* acl.c */
typedef struct pies_acl *pies_acl_t;

struct acl_input
{
  struct sockaddr *addr;
  socklen_t addrlen;
  pies_identity_t identity;
};

pies_acl_t pies_acl_create (const char *name, grecs_locus_t *locus);
void pies_acl_destroy (pies_acl_t *pacl);
void pies_acl_free (pies_acl_t acl);
void pies_acl_use (pies_acl_t acl);
int pies_acl_cmp (struct pies_acl *a, struct pies_acl *b);
int pies_acl_check (pies_acl_t acl, struct acl_input *input, int result);
int parse_acl_line (grecs_locus_t *locus, int allow, pies_acl_t acl,
		    grecs_value_t *value);
pies_acl_t pies_acl_lookup (const char *name);
pies_acl_t pies_acl_install (pies_acl_t acl);

int assert_grecs_value_type (grecs_locus_t *locus,
			     const grecs_value_t *value, int type);

extern struct grecs_keyword acl_keywords[];
extern int acl_section_parser (enum grecs_callback_command cmd,
			       grecs_node_t *node,
			       void *varptr, void *cb_data);
extern int defacl_section_parser (enum grecs_callback_command cmd,
				  grecs_node_t *node,
				  void *varptr, void *cb_data);
