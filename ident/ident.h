/* This file is part of GNU Pies.
   Copyright (C) 2015-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <errno.h>
#include "libpies.h"
#include "grecs.h"
#include "identity.h"

struct pies_identity
{
  pies_identity_provider_t provider;
  char *username;
  void *data;
};

struct pies_identity_mechanism
{
  char const *name;
  int (*authenticate) (pies_identity_provider_t p,
		       pies_identity_t id, char const *passwd);
  int (*is_group_member) (pies_identity_provider_t p,
			  pies_identity_t id, char * const * groups);
  void (*destroy_identity) (pies_identity_provider_t p,
			    pies_identity_t id);
  int (*configure)(struct grecs_node *, pies_identity_provider_t);
  void (*confhelp) (void);
};

struct pies_identity_provider
{
  char *name;
  pies_identity_mechanism_t mech;
  struct grecs_locus locus;
  void *data;
};


