/* This file is part of GNU Pies testsuite.
   Copyright (C) 2019-2025 Sergey Poznyakoff

   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <c-ctype.h>
#include <libpies.h>

enum { LINESIZ = 72 };

static int
next_char (int stop)
{
  static int ch = 0;
  static int i = 0;
  static int c;
  int ret;
  
  switch (i++)
    {
    case 0:
      do
	ch = (ch + 1) % 128;
      while (!c_isprint (ch));
      if (ch == stop)
	return 0;
      c = ch;
      break;
      
    case LINESIZ:
      return '\r';

    case LINESIZ+1:
      i = 0;
      return '\n';
    }

  ret = c;
  do
    c = (c + 1) % 128;
  while (!c_isprint (c));
  
  return ret;
}

int
main (int argc, char **argv)
{
  struct pies_url *url;
  int fd;
  FILE *fp;
  unsigned n;
  int c, first;
  char *progname = argv[0];
  
  if (argc != 2)
    {
      fprintf (stderr, "usage: %s URL\n", progname);
      fprintf (stderr, "Tests the character generator protocol\n");
      return 64;
    }

  if (pies_url_create (&url, argv[1]))
    {
      perror (argv[1]);
      return 64;
    }

  fd = url_connect (url, NULL);
  if (fd == -1)
    return 1;
  fp = fdopen (fd, "r");
  if (!fp)
    {
      perror ("fdopen");
      return 1;
    }
  
  first = next_char (0);
  c = first;
  do
    {
      int in = fgetc (fp);
      if (in == EOF)
	{
	  fprintf (stderr, "%s: unexpected EOF in %u\n", progname, n);
	  return 1;
	}
      if (in != c)
	{
	  fprintf (stderr, "%s: got %d instead of %d in %u\n",
		   progname, in, c, n);
	  return 1;
	}
      n++;
    }
  while ((c = next_char (first)) != 0);
  return 0;
}
	    
      
  
