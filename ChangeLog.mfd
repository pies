2009-08-27  Sergey Poznyakoff  <gray@gnu.org.ua>

	Fix diagnostic and help messages.

	1. Option descriptions in `--help' output do not constitute
	   conceptual sentences. Therefore, they should not begin
	   with a capital letter, nor end with a final point.
	2. According to standards.texi, a diagnostic message in a
	   non-interactive program "should not begin with a capital letter
	   when it follows a program name and/or file name, because that
	   isn't the beginning of a sentence.  (The sentence conceptually
	   starts at the beginning of the line.)  Also, it should not end
	   with a period."

	Affected files:

	* pies/pies.c
	* pies/progman.c

2009-06-17  Sergey Poznyakoff  <gray@gnu.org.ua>

	Bugfixes.

	* pies/pies.c (main): Change main loop to do..while,
	otherwise a signal arriving before entering the loop
	might cause the program to terminate.

2009-06-14  Sergey Poznyakoff  <gray@gnu.org.ua>

	Fix and improve termination actions.

	* pies/pies.h (MAX_RETURN_CODE): Remove.
	(STATUS_SIG_BIT, STATUS_CODE): New defines
	(struct action): New fields next, nstat, status, command.
	(struct component): Keep singly-linked list of termination
	actions.
	* pies/progman.c (run_command): New function.
	(progman_cleanup): Redo iteration over termination actions.
	* pies/pies.c (return_code_cfg_param): Pass offsets in
	struct component.
	(create_action): Accept signal numbers (SIG.* or SIG\+[0-9]+)
	in tag.
	(return_code_section_parser): Update.
	(pies_check_status): Set *pid before returning pies_status_stale.
	* doc/pies.texi: Document changes.

2009-06-04  Sergey Poznyakoff  <gray@gnu.org.ua>

	MeTA1-related improvements.

	* pies/pies.c (_cm_include_meta1): Fix coredump on syntax errors
	in meta1.conf.

2009-05-21  Sergey Poznyakoff  <gray@gnu.org.ua>

	More work on `silent rules' mode

	* pies/Makefile.am: Add silent rule markers.

2009-05-13  Sergey Poznyakoff  <gray@gnu.org.ua>

	Minor change

	* pies/progman.c: Use _exit in child processes.  This
	is a work over a fault in MU design: it registers
	the pidfile removal function via atexit.

2008-12-26  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc/pies.texi: Update.

	git-svn-id: file:///svnroot/mailfromd/trunk@1771
	7a8a7f39-df28-0410-adc6-e0d955640f24

2008-12-01  Sergey Poznyakoff  <gray@gnu.org.ua>

	Documentation and minor fixes.

	* pies/pies.c, pies/progman.c, pies/meta1gram.y, pies/pies.h,
	pies/meta1lex.l: Normalize output redirection terminology, fix the
	debug levels.
	* doc/pies.texi: Document the configuration.

2008-11-21  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c (component_cfg_param): New statement settle-timeout.
	* pies/progman.c (recompute_alarm): New static;
	(prog_start): Start the component only if all of its prerequisites
	are running.
	(progman_recompute_alarm): New function.
	(progman_start): Reset recompute_alarm to 0.
	(check_stopping): New function.
	(progman_wake_sleeping): Handle also status_stopping and
	status_enabled. Recompute alarm before leaving.
	(prog_start_prerequisites): Return 0 or 1 depending on whether all
	prerequisites have been started.
	(prog_stop): Raise recompute_alarm for stopping components.
	(progman_cleanup): Force status_enabled before attepmting to
	start a component.
	(progman_cleanup): Add a \n after debugging message.
	Run progman_wake_sleeping unlsess expect_term is set.
	* pies/meta1gram.y (translate_node_list): Force settle-timeout =
	1.
	* pies/pies.h (struct component.settle_timeout): New member.

	* pies/pies.c (main): New options --force, --dump-prereq and
	--dump-depmap.
	Refuse to start if another instance is already running.
	(pies_check_status): New function.
	(pies_status): Rewrite using pies_check_status.
	* pies/progman.c (component_fixup_depend): Bugfix: move
	mu_list_append off the conditional.
	(progman_dump_prereq, progman_dump_depmap): New functions.
	* pies/meta1gram.y (yyerror): Return 0.
	* pies/pies.h (progman_dump_prereq, progman_dump_depmap): New
	protos.
	(meta1_parser_set_debug, meta1lex, meta1error)
	(meta1parse): Likewise.
	
	* pies/progman.c (print_dep): New function.
	(progman_build_depmap): Call print_dep to facilitate fixing cyclic
	dependencies.
	(prog_start_prerequisites): Fix debugging output.
	Remove spurious second loop (was it a typo or copy-paste
	leftover?)
	(prog_stop_dependents): Fix debugging output.
	(progman_dump_stats): Avoid race conditions between running and
	querying instances.

2008-11-17  Sergey Poznyakoff  <gray@gnu.org.ua>

	Fix handling of retranslators.
	
	* pies/pies.c (main): Call progman_build_depmap and
	progman_create_sockets before starting up components.
	* pies/progman.c (struct prog): New member `retr'.
	(destroy_prog): New function.
	(retr_tag): Change signature. Fix erroneous condition for
	xalloc_die.
	(register_retr): Return prog. Make static. Initialize idx
	with -1.
	(update_retr): Register retranslator, if necessary.
	(progman_register_retranslators): Remove.
	(register_prog0): 2nd argument: index.
	(register_prog): Do not create socket at once.
	(prog_start): Remove debugging fprintf.
	(progman_accept): Update call to register_prog0.
	(progman_create_sockets): New function.
	(progman_start): Remove calls to progman_register_retranslators
	and progman_build_depmap. They must be done only once.
	(prog_stop_retranslators): New function.
	(prog_stop_dependents): Call prog_stop_retranslators.
	(progman_cleanup): Remove dangling retranslators.
	* pies/pies.h (progman_create_sockets): New prototype.

2008-11-16  Sergey Poznyakoff  <gray@gnu.org.ua>

	Improve pies.
	
	* pies/pies.h (create_socket): Change proto to 3 arguments.
	* pies/socket.c (create_socket): Take two additional arguments.
	Allow to configure ownership and mode of created UNIX sockets.
	* pies/progman.c (register_prog, prog_start): Update calls to
	create_socket.

2008-11-15  Sergey Poznyakoff  <gray@gnu.org.ua>

	Improve handling of program privileges.
	
	* pies/pies.h (struct pies_privs_data): Remove
	(struct component): Change type of `privs' to struct mf_privs.
	(priv_setup): Remove.
	* pies/pies.c: Use mf_priv* stuff to manipulate privileges.
	* pies/progman.c: Likewise.
	* pies/meta1gram.y (list): allow for trailing comma.

2008-10-31  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/progman.c, pies/pies.h: Use umaxtostr, instead
	of mu_umaxtostr.

2008-10-15  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/progman.c (env_setup): Port new implementation from Rush.

2008-10-04  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c (component_cfg_param): New configuration statement
	`precious'.
	* pies/progman.c (struct prog): New member `precious'.
	(register_prog): Set precious.
	(prog_start_dependencies): Do not mark disabled precious
	dependencies of a disabled component.
	* pies/pies.h (pies/pies.h): New member `precious'.
	* pies/pies.rcin (mcp): Mark as precious.

	* pies/pies.c (return_code_section_parser): Fix to work with tag
	of any type.
	* pies/pies.rcin: Minor change.

	* pies/pies.c (main): Do cleanup after main action processing and
	only if action == ACTION_CONT.
	* pies/progman.c (struct prog): New member `prev'.
	(link_prog): Maintain doubly-linked list.
	(prog_stop_dependent): Avoid printing diagnostics if there are
	no dependencies to be stopped.
	(prog_stop): Do nothing if prog->pid is zero.
	Do not call prog_stop_dependent, it will be done by
	progman_cleanup. 
	(prog_stop_all): Terminate components from last to first.
	* pies/pies.rcin: Improve example, by adding mcp component.
	
	* pies/pies.c (_cb_depend): Handle single string value.
	(_cb_retr): Fix diagnostic message.
	(_cb_facility): New function.
	(component_cfg_param): New statement `facility'.
	(component_section_parser): Initialize comp.facility to the
	default value.
	* pies/progman.c (struct prog): New member `facility'.
	(register_prog): Initialize facility.
	(open_retranslator): Use master->facility.
	* pies/pies.h (struct component): New member `facility'.

2008-10-03  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/progman.c (register_prog): Remove stray semicolon.
	(retr_exit): Use _exit.
	(prog_stop_dependent): Bugfix: stop retranlators as well as
	components.

2008-09-29  Sergey Poznyakoff  <gray@gnu.org.ua>

        * pies/pies.c: Rewrite configuration
	file support to match changes in Mailutils.

2008-09-09  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c: Change description.

2008-06-26  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.h: Include stdint.h

2008-02-26  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c: New configuration statement "chdir".
	* pies/progman.c (struct prog.v.p): New member `dir'.
	(register_prog): Initialize newp->v.p.dir;
	(prog_start): Change to the working directory, if required by
	config.

2008-02-01  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c (progman_wake_disabled): Renamed to
	progman_wake_sleeping.
	* pies/progman.c: Implement proper handling of dependencies.
	* pies/pies.h ((progman_wake_disabled): Renamed to
	progman_wake_sleeping.
	(pies_depmap_t,pies_depmap_pos_t,enum pies_depmap_direction): New
	types.
	(depmap_alloc,depmap_copy,depmap_set,depmap_isset,depmap_tc)
	(depmap_first,depmap_next): New functions.
	* pies/depmap.c: New file.
	* pies/Makefile.am (pies_SOURCES): Add depmap.c.

	* pies/pies.c (return_code_section_parser): Allow to use symbolic
	exit codes.
	(priv_setup): Exit with EX_CONFIG if the user does not exist.
	(main): Catch early sigchlds as well.
	* pies/progman.c (progman_cleanup): Fix notice wording.
	* pies/pies.rcin: Add sample return-code section.
	(component pmult): Redirect stderr/stdout to syslog.

2008-01-31  Sergey Poznyakoff  <gray@gnu.org.ua>

	Implement configurable actions depending on the exit code of
	a component.
	
	* pies/pies.c: New configuration file section "return-code".
	(capa): Add mailer and debug.
	(main): Register "return-code" section and all mailer formats.
	* pies/progman.c (enum prog_status): New type.
	(struct prog): Remove `disabled'. New fields `status' and `act'.
	(register_prog): Initialize act.
	(prog_start): Exit with EX_SOFTWARE code if the component cannot
	be started.
	(progman_start): Do not start disabled components.
	(progman_wake_disabled): Wake only components marked as
	status_stopped.
	(notify): New function.
	(progman_cleanup): Act in accordance with the `act' field if the
	component terminates.
	(progman_stop_component,progman_dump_stats): Take into account
	prog->v.p.status.
	* pies/pies.h (MAX_RETURN_CODE): New define.
	(enum return_action): New type.
	(struct component.act): New member.
	(default_component): New global.

2008-01-24  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c: Fix handling of the --log-tag option.

	* pies/progman.c (prog_start): Remove unconditional debugging
	output.

2008-01-22  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c (log_setup): Always set mu_debug_default_printer to
	override bug in libmailutils.
	(component_cfg_param): New keyword `env'.
	(main): Bugfix: It is -R that allows arguments, not -r.
	* pies/progman.c (struct prog): New member env.
	(register_prog): Avoid unnecessary allocations. Copy env.
	(env_setup): New function.
	(prog_start): Set up environment.
	* pies/pies.h (struct component): New member env.

2008-01-14  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c, pies/progman.c,
	pies/pies.h, pmult/pmult.c: Sync with MU CVS: Use mu_log_facility
	and mu_log_tag.

2008-01-13  Sergey Poznyakoff  <gray@gnu.org.ua>

	* pies/pies.c (_cb_group): Bugfix: store group number in the list,
	not its name.
	(component_section_parser): Initialize retr to default values.
	* pies/Makefile.am (LDADD): Place libgnu.a after
	MAILUTILS_LIBS. 
	
	* pies: New directory.
	* pies/Makefile.am: New file.
	* pies/pies.c, pies/progman.c, pies/pies.h, pies/pies.rcin: New
	files.

Local Variables:
mode: change-log
version-control: never
buffer-read-only: t
End:
