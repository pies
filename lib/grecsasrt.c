/* This file is part of GNU Pies
   Copyright (C) 2015-2025 Sergey Poznyakoff
  
   GNU Pies is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Pies is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Pies.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <errno.h>
#include <grecs.h>
#include "libpies.h"
#include "grecsasrt.h"

int
conf_callback_url (enum grecs_callback_command cmd,
		   grecs_node_t *node,
		   void *varptr, void *cb_data)
{
  grecs_locus_t *locus = &node->locus;
  grecs_value_t *value = node->v.value;
  struct pies_url *url;

  if (grecs_assert_node_value_type (cmd, node, GRECS_TYPE_STRING))
    return 1;
  if (pies_url_create (&url, value->v.string))
    {
      grecs_error (locus, 0, _("%s: cannot create URL: %s"),
		    value->v.string, strerror (errno));
      return 0;
    }
  if (varptr)
    *(struct pies_url **) varptr = url;
  else
    pies_url_destroy (&url);
  return 0;
}

